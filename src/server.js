Promise = require('bluebird'); // eslint-disable-line no-global-assign

const mongoose = require('./core/mongoose');
const app = require('./core/express');
const logger = require('./core/logger');
const { APP_PORT, APP_HOST, APP_ENV, MONGO } = require('./config/config');

/**
 * Initialize server
 */
(async () => {
    try {
        // Connect MongoDB
        const dbConnection = await mongoose.connect(MONGO.URI);
        logger.info(`✅ Database initialized... (${dbConnection.name})`);
        // Run express app
        await app.run(APP_PORT);
        logger.info(`✅ Server listened at ${APP_HOST}:${APP_PORT} (${APP_ENV})`);
    } catch (err) {
        logger.error(err);
        process.exit(1);
    }
})();

/**
 * Module exports.
 * @public
 */
module.exports = app;
