const axios = require('axios');
const { AppError } = require('../error');
const AuthError = require('../../api/auth/auth.error');
const { LINKEDIN_USER_PROFILE, LINKEDIN_USER_EMAIL } = require('../../utils/constants');

/**
 * @param {Object} token - Token linked in from client
 * @public
 */
exports.getLinkedInUserFromToken = async (token) => {
    try {
        const headers = {
            Authorization: `Bearer ${token}`,
        };

        const [{ data: userInfo }, { data: userEmail }] = await Promise.all([
            axios.get(LINKEDIN_USER_PROFILE, { headers }),
            axios.get(LINKEDIN_USER_EMAIL, { headers }),
        ]);

        const email = userEmail.elements[0]['handle~'].emailAddress;
        const prefix = `${userInfo.firstName.preferredLocale.language}_${userInfo.firstName.preferredLocale.country}`;
        const name = `${userInfo.firstName.localized[prefix]} ${userInfo.lastName.localized[prefix]}`;
        const picture = userInfo.profilePicture['displayImage~'].elements[1].identifiers[0].identifier;
        return { email, name, picture, email_verified: true };
    } catch (error) {
        throw new AppError(AuthError.InvalidAccessToken);
    }
};
