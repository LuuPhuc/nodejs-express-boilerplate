const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */
module.exports = Object.freeze({
    UnknownError: {
        message: 'Unknown error',
        code: 0,
        status: httpStatus.INTERNAL_SERVER_ERROR,
        isPublic: false,
    },
    NotFound: {
        message: 'Not Found',
        code: 1,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
    ValidationFailed: {
        message: 'Validation failed',
        code: 2,
        status: httpStatus.BAD_REQUEST,
        isPublic: true,
    },
    NoTokenProvided: {
        message: 'No token provided',
        code: 3,
        status: httpStatus.UNAUTHORIZED,
        isPublic: true,
    },
    AccessTokenExpired: {
        message: 'Access token expired',
        code: 4,
        status: httpStatus.UNAUTHORIZED,
        isPublic: true,
    },
    InvalidAccessToken: {
        message: 'Invalid access token',
        code: 5,
        status: httpStatus.UNAUTHORIZED,
        isPublic: true,
    },
    TransactionFail: {
        message: 'Transaction Fail',
        code: 6,
        status: httpStatus.NOT_ACCEPTABLE,
        isPublic: true,
    },
});
