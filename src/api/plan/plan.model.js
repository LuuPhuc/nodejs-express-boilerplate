const mongoose = require('mongoose');
const { CARD_STATUS } = require('../../utils/constants');
const { transformMongooseDocumentToObject: transform } = require('../../utils/utils');

/**
 * plan schema
 * @private
 */
const Schema = new mongoose.Schema(
    {
        plan_ref_id: { type: String },
        name: { type: String },
        description: { type: String },
        status: { type: String, enum: Object.values(CARD_STATUS), default: CARD_STATUS.ACTIVE },
        price_per_member: { type: Number },
        currency: { type: String }, //	e.g. USD, VND
        duration: { type: Number }, //	Duration (days)
        sort_order: { type: Number }, // Using for display sort
        min_cost: { type: Number },
        min_price: { type: Number },
        free_trial_days: { type: Number },
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } },
);

/**
 * Methods
 */
Schema.method({});

/**
 * Statics
 */
Schema.statics = {
    CARD_STATUS,
    /**
     * Get plan by ID
     * @param {string} id - plan ID
     */
    async getById(id) {
        if (!mongoose.Types.ObjectId.isValid(id)) return undefined;
        const plan = await this.findOne({ _id: id }).lean();
        return transform(plan);
    },

    /**
     * Create a new plan
     * @param {Object} planDTO
     */
    async createPlan(planDTO) {
        const plan = await this.create(planDTO);
        return transform(plan.toObject());
    },
};

/**
 * Module exports
 * @public
 */
module.exports = mongoose.model('Plan', Schema, 'plans');
