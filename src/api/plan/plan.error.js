const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */

module.exports = Object.freeze({
    PlanNotFound: {
        message: 'Plan not found',
        code: 15001,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
    PlanAlreadyExist: {
        message: 'Plan already exists',
        code: 15002,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
});
