const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */
module.exports = Object.freeze({
    ClientNotFound: {
        message: 'Client not found',
        code: 5001,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
    ClientAlreadyExists: {
        message: 'Client already exists',
        code: 5002,
        status: httpStatus.CONFLICT,
        isPublic: true,
    },
});
