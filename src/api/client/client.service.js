// Library
const _ = require('lodash');

// Model
const ClientModel = require('mongoose').model('Client');

// Service
const OrganizationService = require('../organization/organization.service');

// Errors
const OrganizationError = require('../organization/organization.error');
const ClientError = require('./client.error');
const { AppError } = require('../../core/error');

// Constant
const { CLIENT_STATUS } = require('../../utils/constants');

// Transform client
const clientTransform = [
    'id',
    'organization_id',
    'name',
    'email',
    'status',
    'description',
    'address',
    'budget',
    'currency',
    'net_terms',
    'invoice_note',
    'created_at',
];

/** CRUD SERVICE */

/**
 * Get client by id
 * @param {String} clientId
 * @param {String} organizationId
 */
exports.getClientById = async (clientId, organizationId) => {
    const client = await ClientModel.findOne({
        _id: clientId,
        organization_id: organizationId,
    });

    if (_.isNull(client)) {
        throw new AppError(ClientError.ClientNotFound);
    }
    // Transform client
    const transformedClient = _.pick(client, clientTransform);
    return transformedClient;
};

/**
 * Get client by conditions
 * @param {Object} conditions
 */
exports.getClientByConditions = async (conditions) => {
    const client = await ClientModel.find({
        ...conditions,
    });

    // Check exists client
    if (_.isNull(client)) {
        throw new AppError(ClientError.ClientNotFound);
    }

    // Transform client
    const transformedClient = _.pick(client, clientTransform);
    return transformedClient;
};

/**
 * Creates a clients
 * @param {Object} clientDTO
 */
exports.createClient = async (organizationId, clientDTO) => {
    // Check exists organization
    const organization = await OrganizationService.getOrganizationById(organizationId);
    if (_.isNull(organization)) {
        throw new AppError(OrganizationError.OrganizationNotFound);
    }
    // Check exists client
    const existsClient = await ClientModel.findOne({
        organization_id: organizationId,
        email: clientDTO.email,
        status: CLIENT_STATUS.ACTIVE,
    });
    if (!_.isNull(existsClient)) {
        throw new AppError(ClientError.ClientAlreadyExists);
    }
    // Create new client
    const newClient = await ClientModel.create({
        organization_id: organizationId,
        ...clientDTO,
    });

    // Transform client
    const transformedClient = _.pick(newClient, clientTransform);
    return transformedClient;
};

/**
 * Delete client
 * @param {String} organizationId
 * @param {String} clientId
 * @param {String} status
 */
exports.deleteClient = async (clientId, organizationId) => {
    // Check exists organization
    await OrganizationService.getOrganizationById(organizationId);

    const client = await ClientModel.findOneAndUpdate(
        { _id: clientId, organization_id: organizationId },
        { $set: { status: CLIENT_STATUS.INACTIVE } },
        { new: true },
    );
    if (_.isNull(client)) {
        throw new AppError(ClientError.ClientNotFound);
    }
};
