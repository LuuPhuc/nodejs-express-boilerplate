const controller = require('./upload.controller');
const authenticate = require('../../core/middleware/authenticate');

/**
 * Module exports
 * @public
 */
module.exports = [
    /** Upload image */
    {
        path: '/upload/image',
        method: 'post',
        middlewares: [authenticate],
        controller: controller.uploadImage,
    },
];
