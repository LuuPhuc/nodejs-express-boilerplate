const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */
module.exports = Object.freeze({
    ImageNotFound: {
        message: 'Image not found',
        code: 23001,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
});
