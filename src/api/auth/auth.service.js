// Library
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const ms = require('ms');
const jwt = require('jsonwebtoken');
const { v4: uuidV4 } = require('uuid');
const { Duration, DateTime } = require('luxon');

// Model
const UserModel = require('mongoose').model('User');
const AuthSessionModel = require('mongoose').model('AuthSession');

// Services
const RedisService = require('../../core/services/redis');

// Error
const { AppError } = require('../../core/error');
const UserError = require('../user/user.error');
const AuthError = require('./auth.error');

// Constants
const Utils = require('../../utils/utils');
const { ACCESS_TOKEN_EXP, REFRESH_TOKEN_EXP, JWT_SECRET } = require('../../config/config');
const { REDIS_EXP_SECRET } = require('../../utils/constants');

const accessTokenDuration = Duration.fromMillis(ms(ACCESS_TOKEN_EXP));
const refreshTokenDuration = Duration.fromMillis(ms(REFRESH_TOKEN_EXP));

const userTransform = ['id', 'name', 'email', 'email_verified', 'picture', 'type', 'status', 'created_at'];

/**
 * Hash user password
 *
 * @param {String} pwd - Password
 * @return {Promise<string>} Hashed password
 * @private
 */
const hashPassword = async (pwd) => bcrypt.hash(pwd, 12);

/**
 * Verify user password
 *
 * @param {String} pwd - Password
 * @param {String} hash - Hashed password
 * @return {Boolean}
 * @private
 */
const verifyPassword = async (pwd, hash) => {
    const result = await bcrypt.compare(pwd, hash);
    return !!result;
};

/**
 * Generate Bearer tokens | Include "access token" and "refresh token"
 *
 * @param {Object} user - User info
 * @private
 */
const generateTokens = (user) => {
    const accessToken = jwt.sign(
        {
            sub: user.id,
            name: user.name,
            email: user.email,
            email_verified: user.email_verified,
            type: user.type,
            exp: DateTime.utc().plus(accessTokenDuration).toSeconds(),
        },
        JWT_SECRET,
    );
    const refreshToken = `${user.id}#${uuidV4().replace(/-/g, '')}`;
    return {
        token_type: 'Bearer',
        access_token: accessToken,
        expires_in: accessTokenDuration.as('seconds'),
        refresh_token: refreshToken,
    };
};

/**
 * Generate new auth session
 *
 * @param {Object} user - User info
 * @param {Object} client - Client info
 * @private
 */
const generateAuthSession = async ({ user, client }) => {
    const tokens = generateTokens(user);
    const authSessionRecord = await AuthSessionModel.create({
        user_id: user.id,
        ip: client.ip,
        device: client.agent,
        refresh_token: tokens.refresh_token,
        expires: DateTime.utc().plus(refreshTokenDuration).toJSON(),
    });
    const authSession = authSessionRecord.transform();
    return {
        tokens,
        session: authSession,
    };
};

/**
 * Register a new user
 *
 * @param {Object} user - User info
 * @param {Object} client - Client info
 * @public
 */
exports.register = async ({ user, client }) => {
    // Check duplicate user email
    if (await UserModel.countDocuments({ email: user.email })) {
        throw new AppError(AuthError.EmailAlreadyTaken);
    }
    // Hash password
    const hashedPwd = await hashPassword(user.password);
    // Create new user
    const newUserRecord = await UserModel.create({
        ...user,
        username: uuidV4().replace(/-/g, ''),
        password: hashedPwd,
    });
    const newUser = newUserRecord.transform();
    // Create new session
    const { tokens } = await generateAuthSession({ user: newUser, client });
    return { user: newUser, tokens };
};

/**
 * Login with email and password
 *
 * @param {String} id - User email or username
 * @param {String} password - User password
 * @param {Object} client - Client info
 * @public
 */
exports.login = async ({ id, password, client }) => {
    // Get existing user by email or username
    const user = await UserModel.getByEmailOrUsername(id);
    // Check user password
    if (!user || !(await verifyPassword(password, user.password))) {
        throw new AppError(AuthError.IncorrectIdOrPassword);
    }
    const transformedUser = _.pick(user, userTransform);
    // Create new session
    const { tokens } = await generateAuthSession({ user: transformedUser, client });
    return { user: transformedUser, tokens };
};

/**
 *
 * @param {*} userInfo
 * @param {*} client
 * @public
 */
exports.loginByOAuth2 = async (userInfo, client) => {
    const userRecord = await UserModel.findOne({ email: userInfo.email });
    if (!userRecord) {
        // Create new user
        const user = await UserModel.create({
            email: userInfo.email,
            picture: userInfo.picture,
            email_verified: true,
            username: uuidV4().replace(/-/g, ''),
        });
        const newUser = user.transform();
        const { tokens } = await generateAuthSession({ user: newUser, client });
        return { user: newUser, tokens };
    }
    const user = userRecord.transform();
    // Create new session
    const { tokens } = await generateAuthSession({ user, client });
    return { user, tokens };
};

/**
 * Refresh token
 *
 * @param {string} userID - User ID
 * @param {String} refreshToken - Refresh token
 * @param {Object} client - Client info
 * @public
 */
exports.refresh = async ({ userID, refreshToken, client }) => {
    // Find user and auth session
    const [userRecord, authSessionRecord] = await Promise.all([
        UserModel.findById(userID),
        AuthSessionModel.findOne({ refresh_token: refreshToken }),
    ]);
    // Check valid token
    if (!userRecord || !authSessionRecord || DateTime.fromISO(authSessionRecord.expires) < DateTime.utc()) {
        throw new AppError(AuthError.InvalidRefreshToken);
    }
    // Transform user
    const user = userRecord.transform();
    // Create new session
    const tokens = await generateTokens(user);
    // Update auth session
    authSessionRecord.ip = client.ip;
    authSessionRecord.client = client.agent;
    authSessionRecord.refresh_token = tokens.refresh_token;
    authSessionRecord.expires = DateTime.utc().plus(refreshTokenDuration).toJSON();
    await authSessionRecord.save();
    return tokens;
};

/**
 * Verify account
 * @param {String} userEmail
 */
exports.verifyAccount = async (userEmail) => {
    // Change email verified is true
    const user = await UserModel.findOneAndUpdate(
        {
            email: userEmail,
            email_verified: false,
        },
        {
            $set: {
                email_verified: true,
            },
        },
        {
            new: true,
        },
    );
    if (_.isNull(user)) {
        throw new AppError(UserError.UserNotFound);
    }
};

/**
 * Forgot password
 * @param {String} userEmail
 */
exports.forgotPassword = async (userEmail) => {
    const user = await UserModel.findOne({ email: userEmail, email_verified: true });

    // Check exists email
    if (_.isNull(user)) {
        throw new AppError(UserError.UserNotFound);
    }

    // Gen code
    const code = await Utils.genSecretCode();

    // Write to Redis
    await RedisService.setKeyToRedis(`user_reset_password_${user.id}`, 'reset_password_code', code, REDIS_EXP_SECRET);

    // Send email
};

/**
 * Reset password
 * @param {String} code
 * @param {String} newPassword
 * @param {String} userEmail
 */

// exports.resetPassword = async (code, newPassword, userEmail) => {
//     const user = await UserModel.findOne({ email: userEmail, email_verified: true });

//     // Check exists email
//     if (isNull(user)) {
//         throw new AppError(UserError.UserNotFound);
//     }
//     const verify = await VerifyService.getVerifyByCode({
//         user_email: userEmail,
//         type: CODE.FORGOT_PASSWORD,
//         code,
//     });

//     // Verify code
//     if (isNull(verify)) {
//         throw new AppError(errors.InvalidCode);
//     }

//     if (await verifyPassword(newPassword, user.password)) {
//         throw new AppError(AuthError.NewPasswordBeTheSameAsOldPassword);
//     }
//     // Hash new password
//     const newPasswordHash = await hashPassword(newPassword);

//     // Change new password and delete key at Redis
//     await Promise.all([
//         UserModel.findOneAndUpdate(
//             {
//                 email: userEmail,
//                 status: STATUS.ACTIVE,
//             },
//             {
//                 $set: {
//                     password: newPasswordHash,
//                 },
//             },
//             { new: true },
//         ),
//         VerifyService.clearVerify({
//             _id: verify._id,
//             user_email: user.email,
//             type: CODE.FORGOT_PASSWORD,
//         }),
//     ]);

// Send email confirm
//   agenda.sendEmail({
//     templateName: TEMPLATE_NAME.CONFIRM_PASSWORD_CHANGED,
//     templateContent: [{}],
//     user: {
//       email: user.email,
//       name: user.name,
//     },
//     messageConditions: {
//       subject: SUBJECT.CONFIRM_PASSWORD_CHANGED,
//       merge_vars: [
//         {
//           rcpt: user.email,
//           vars: [
//             {
//               name: TEMPLATE_PARAMS.CONFIRM_PASSWORD_CHANGED.USER_FULLNAME,
//               content: user.name,
//             },
//           ],
//         },
//       ],
//     },
//   });
// };
