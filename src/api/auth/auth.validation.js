const { validate, Joi, Segments } = require('../../core/validation');

module.exports = {
    register: validate({
        [Segments.BODY]: Joi.object({
            name: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().min(6).required(),
        }),
    }),
    login: validate({
        [Segments.BODY]: Joi.object({
            id: Joi.string().required(),
            password: Joi.string().required(),
        }),
    }),
    loginByOauth2: validate({
        [Segments.BODY]: Joi.object({
            token: Joi.string().required(),
        }),
    }),
    refresh: validate({
        [Segments.BODY]: Joi.object({
            refresh_token: Joi.string().required(),
        }),
    }),
};
