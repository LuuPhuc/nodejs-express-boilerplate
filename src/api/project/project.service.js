const _ = require('lodash');
const ProjectModel = require('mongoose').model('Project');

// Constants
const { PROJECT_STATUS } = require('../../utils/constants');

// Error
const ProjectError = require('./project.error');
const { AppError } = require('../../core/error');

// Transform project
const projectTransform = [
    'id',
    'description',
    'name',
    'status',
    'total_budget_limit',
    'daily_budget_limit',
    'weekly_budget_limit',
    'monthly_budget_limit',
    'created_at',
    'user_email',
    'pay_rate',
    'bill_rate',
];

/**
 * Get project by id
 * @param {string} organizationId
 * @param {string} projectId
 */
exports.getProjectById = async (organizationId, projectId) => {
    const project = await ProjectModel.findOne({
        _id: projectId,
        organization_id: organizationId,
        status: PROJECT_STATUS.ACTIVE,
    });

    // Check exists project
    if (_.isNull(project)) {
        throw new AppError(ProjectError.ProjectNotFound);
    }
    const transformedProject = _.pick(project, projectTransform);
    return transformedProject;
};

/**
 * Delete project
 * @param {string} organizationId
 * @param {string} projectId
 */
exports.deleteProject = async (organizationId, projectId) => {
    const project = await ProjectModel.findOneAndUpdate(
        { _id: projectId, organization_id: organizationId },
        { $set: { status: PROJECT_STATUS.INACTIVE } },
        { new: true },
    );
    // Check exists project
    if (_.isNull(project)) {
        throw new AppError(ProjectError.ProjectError);
    }
};
