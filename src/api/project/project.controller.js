const httpStatus = require('http-status');
const ProjectService = require('./project.service');

/**
 * Get project by id
 */
exports.getProjectById = async (req, res, next) => {
    try {
        const { organization_id: organizationId, project_id: projectId } = req.params;

        const project = await ProjectService.getProjectById(organizationId, projectId);
        return res.status(httpStatus.OK).json(project);
    } catch (err) {
        return next(err);
    }
};

/**
 * Delete project
 */
exports.deleteProject = async (req, res, next) => {
    try {
        const { organization_id: organizationId, project_id: projectId } = req.params;

        await ProjectService.deleteProject(organizationId, projectId);
        return res.status(httpStatus.NO_CONTENT).json();
    } catch (err) {
        return next(err);
    }
};
