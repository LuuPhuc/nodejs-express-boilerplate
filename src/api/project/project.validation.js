const { validate, Joi, Segments } = require('../../core/validation');
const { OBJECT_ID_PATTERN } = require('../../utils/constants');

module.exports = {
    getProjectById: validate({
        [Segments.PARAMS]: Joi.object({
            organization_id: Joi.string().regex(OBJECT_ID_PATTERN).required(),
            project_id: Joi.string().regex(OBJECT_ID_PATTERN).required(),
        }),
    }),
    deleteProject: validate({
        [Segments.PARAMS]: Joi.object({
            organization_id: Joi.string().regex(OBJECT_ID_PATTERN).required(),
            project_id: Joi.string().regex(OBJECT_ID_PATTERN).required(),
        }),
    }),
};
