const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */

module.exports = Object.freeze({
    ProjectNotFound: {
        message: 'Project not found',
        code: 16001,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
    ProjectAlreadyExits: {
        message: 'Project already exits',
        code: 16002,
        status: httpStatus.CONFLICT,
        isPublic: true,
    },
    OrganizationGuestCanBecomeOnlyProjectViewer: {
        message: ' Organization guest  can  become only Project viewer',
        code: 16003,
        status: httpStatus.BAD_REQUEST,
        isPublic: true,
    },
});
