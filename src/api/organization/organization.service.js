// Library
const _ = require('lodash');

// Model
const OrganizationModel = require('mongoose').model('Organization');

// Constants
const { ORGANIZATION_STATUS } = require('../../utils/constants');

// Error
const OrganizationError = require('./organization.error');
const { AppError } = require('../../core/error');

// Transform organization
const organizationTransform = [
    'id',
    'created_by_id',
    'name',
    'status',
    'picture',
    'address',
    'phone_number',
    'tax_number',
    'industry',
    'team_size_from',
    'team_size_to',
    'contact',
    'invite_url',
    'created_at',
];

/**
 * Get organization by id
 * @param {String} organizationId
 */
exports.getOrganizationById = async (organizationId) => {
    const organization = await OrganizationModel.findById(organizationId);
    if (_.isNull(organization)) {
        throw new AppError(OrganizationError.OrganizationNotFound);
    }
    const transformedOrganization = _.pick(organization, organizationTransform);
    return transformedOrganization;
};

/**
 * Create a organization
 * @param {Object} organizationDto
 */
exports.createOrganization = async (createdById, userEmail, organizationDto) => {
    // Check exists organization
    const existsOrganization = await OrganizationModel.findOne({
        created_by_id: createdById,
        name: organizationDto.name,
    });
    if (!_.isNull(existsOrganization)) {
        throw new AppError(OrganizationError.OrganizationAlreadyExists);
    }

    // Create new organization
    const newOrganization = await OrganizationModel.create({ created_by_id: createdById, ...organizationDto });
    const transformedOrganization = _.pick(newOrganization, organizationTransform);

    return transformedOrganization;
};

/**
 * Update info organization
 * @param {String} organizationId
 * @param {Object} organizationDto
 */
exports.updateInfoOrganization = async (organizationId, organizationDto) => {
    const updatedOrganization = await OrganizationModel.findOneAndUpdate(
        { _id: organizationId },
        { $set: organizationDto },
        { new: true },
    );

    // Check exists organization
    if (_.isNull(updatedOrganization)) {
        throw new AppError(OrganizationError.OrganizationNotFound);
    }

    const transformedOrganization = _.pick(updatedOrganization, organizationTransform);
    return transformedOrganization;
};

/**
 * Delete organization
 * @param {String} organizationId
 */
exports.deleteOrganization = async (organizationId) => {
    const organization = await OrganizationModel.findOneAndUpdate(
        { _id: organizationId },
        { $set: { status: ORGANIZATION_STATUS.INACTIVE } },
        { new: true },
    );
    if (_.isNull(organization)) {
        throw new AppError(OrganizationError.OrganizationNotFound);
    }
};
