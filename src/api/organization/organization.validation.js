const { validate, Joi, Segments } = require('../../core/validation');
const { OBJECT_ID_PATTERN } = require('../../utils/constants');

module.exports = {
    getOrganizationById: validate({
        [Segments.PARAMS]: Joi.object({
            organization_id: Joi.string().regex(OBJECT_ID_PATTERN).required(),
        }),
    }),
    createOrganization: validate({
        [Segments.BODY]: Joi.object({
            name: Joi.string().min(1).max(128).required(),
            address: Joi.string().min(1).max(512),
            picture: Joi.string(),
            phone_number: Joi.string().min(6).max(16).required(),
            tax_number: Joi.string(),
            industry: Joi.string(),
            team_size_from: Joi.number(),
            team_size_to: Joi.number(),
            contact: Joi.object({
                name: Joi.string().min(1).max(128).required(),
                email: Joi.string().email().min(1).max(128).required(),
                phone_number: Joi.string().min(6).max(16),
            }),
        }),
    }),
    updateInfoOrganization: validate({
        [Segments.PARAMS]: Joi.object({
            organization_id: Joi.string().regex(OBJECT_ID_PATTERN).required(),
        }),
        [Segments.BODY]: Joi.object({
            name: Joi.string().min(1).max(128),
            address: Joi.string().min(1).max(512),
            picture: Joi.string(),
            phone_number: Joi.string().min(6).max(16),
            tax_number: Joi.string(),
            industry: Joi.string(),
            team_size_from: Joi.number().min(1),
            team_size_to: Joi.number().min(1),
            contact: Joi.object({
                name: Joi.string().min(1).max(128).required(),
                email: Joi.string().email().min(1).max(128).required(),
                phone_number: Joi.string().min(6).max(16),
            }),
        }),
    }),
    deleteOrganization: validate({
        [Segments.PARAMS]: Joi.object({
            organization_id: Joi.string().regex(OBJECT_ID_PATTERN).required(),
        }),
    }),
};
