const request = require('supertest');
const mongoose = require('../../../core/mongoose');
const app = require('../../../core/express');
const logger = require('../../../core/logger');
const { APP_PORT, APP_HOST, APP_ENV, MONGO } = require('../../../config/config');
const TestUtilities = require('../../../utils/testUtilities');

logger.level = 'off';

// Admin
const adminDto = {
    email: 'adminUser@gmail.com',
    password: '123456',
    name: 'The Nice',
    type: 'admin',
};

// Normal user
const userDto = {
    email: 'user@gmail.com',
    password: '123456',
    name: 'user',
    type: 'user',
};

// Access denied - organization-guest
const userAccessDenied = {
    email: 'accessdenied@gmail.com',
    password: '123456',
};

// Organization
const organizationDto = {
    name: 'The Nice Group DTO',
    picture: 'Beautiful picture',
    address: '151 Dao Duy Anh phuong 9 quan Phu Nhuan',
    phone_number: '0356245247',
    tax_number: '123956734',
    industry: 'IT',
    team_size_from: 30,
    team_size_to: 50,
    contact: {
        name: 'CEO',
        email: 'adminUser@gmail.com',
        phone_number: '035624524443',
    },
};

// Organization not found
const organizationIdNotFound = '5f4cbd1454b07c35f2f293f2';

// Membership
const membershipDto = {
    user_email: 'accessdenied@gmail.com',
    role: 'organization_guest',
    status: 'active',
};

describe('Organizations API Failure', () => {
    let dbConnection = '';
    let server = {};
    let admin = {};
    let user = {};
    let organization = {};
    let membership = {};
    let accessDenied = {};

    beforeAll(async () => {
        try {
            jest.setTimeout(30000);
            // Connect MongoDB
            dbConnection = await mongoose.connect(MONGO.URI);
            logger.info(`✅ Database initialized... (${dbConnection.name})`);
            // Run express app
            server = app.listen(APP_PORT);
            logger.info(`✅ Server listened at ${APP_HOST}:${APP_PORT} (${APP_ENV})`);
        } catch (err) {
            logger.error(err);
            process.exit(1);
        }

        try {
            // Login  with admin role
            [admin, user, accessDenied] = await Promise.all([
                TestUtilities.createUser(adminDto),
                TestUtilities.createUser(userDto),
                TestUtilities.createUser(userAccessDenied),
            ]);
            organization = await TestUtilities.createOrganizationByCreatedById(admin.user.id, organizationDto);
            membership = await TestUtilities.createMembership(organization.id, membershipDto);
        } catch (err) {
            await deleteData();
        }
    });

    // Close when done
    afterAll(async (done) => {
        await deleteData();
        server.close();
        done();
    });

    // Clear all data when done test
    const deleteData = () =>
        Promise.all([
            TestUtilities.deleteUser([admin.user.id, user.user.id]),
            TestUtilities.deleteUser([accessDenied.user.id, accessDenied.user.id]),
            TestUtilities.deleteMembership(membership.id, organization.id),
            TestUtilities.deleteOrganization(admin.user.id, organization.id),
        ]);

    /** GET ORGANIZATIONS BY ID */
    describe('GET /v1/organizations/:organization_id', () => {
        // Organization not found
        it('should return organizations not found', async () => {
            const response = await request(server)
                .get(`/v1/organizations/${organizationIdNotFound}`)
                .set('Authorization', `Bearer ${admin.tokens.access_token}`);
            expect(response.statusCode).toBe(404);
            expect(response.body).toEqual({
                error: {
                    message: 'Organization not found',
                    code: 12001,
                },
            });
        });

        // Member not exist in Organization
        it('should return error member not exists in organization', async () => {
            const response = await request(server)
                .get(`/v1/organizations/${organization.id}`)
                .set('Authorization', `Bearer ${user.tokens.access_token}`);
            expect(response.body).toEqual({
                error: {
                    message: 'Member not exist in Organization',
                    code: 11001,
                },
            });
            expect(response.statusCode).toBe(401);
        });

        // Access denied
        it('should return error access denied', async () => {
            const response = await request(server)
                .get(`/v1/organizations/${organization.id}`)
                .set('Authorization', `Bearer ${accessDenied.tokens.access_token}`);
            expect(response.body).toEqual({
                error: {
                    message: 'Access denied',
                    code: 6,
                },
            });
        });

        // Miss auth
        it('should return error no token provided', async () => {
            const response = await request(server).get(`/v1/organizations/${organization.id}`);
            expect(response.body).toEqual({
                error: {
                    message: 'No token provided',
                    code: 3,
                },
            });
        });
    });
});
