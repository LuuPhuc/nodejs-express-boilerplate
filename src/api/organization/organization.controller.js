const httpStatus = require('http-status');
const OrganizationService = require('./organization.service');
// const { PAGINATION } = require('../../utils/constants');

/**
 * Get organization by id
 */
exports.getOrganizationById = async (req, res, next) => {
    try {
        const { organization_id: organizationId } = req.params;
        const organization = await OrganizationService.getOrganizationById(organizationId);
        return res.status(httpStatus.OK).json(organization);
    } catch (err) {
        return next(err);
    }
};

/**
 * Creates a organization
 */
exports.createOrganization = async (req, res, next) => {
    try {
        const { id, email } = req.auth;
        const newOrganization = await OrganizationService.createOrganization(id, email, req.body);
        return res.status(httpStatus.CREATED).json(newOrganization);
    } catch (err) {
        return next(err);
    }
};

/**
 * Update info organization
 */
exports.updateInfoOrganization = async (req, res, next) => {
    try {
        const { organization_id: organizationId } = req.params;

        const organization = await OrganizationService.updateInfoOrganization(organizationId, req.body);

        return res.status(httpStatus.OK).json(organization);
    } catch (err) {
        return next(err);
    }
};

/*
 * Delete organization
 */
exports.deleteOrganization = async (req, res, next) => {
    const { organization_id: organizationId } = req.params;
    try {
        await OrganizationService.deleteOrganization(organizationId);
        return res.status(httpStatus.NO_CONTENT).json();
    } catch (err) {
        return next(err);
    }
};
