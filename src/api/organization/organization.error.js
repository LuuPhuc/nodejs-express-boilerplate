const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */
module.exports = Object.freeze({
    OrganizationNotFound: {
        message: 'Organization not found',
        code: 12001,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
    OrganizationAlreadyExists: {
        message: 'Organization already exists',
        code: 12002,
        status: httpStatus.CONFLICT,
        isPublic: true,
    },
});
