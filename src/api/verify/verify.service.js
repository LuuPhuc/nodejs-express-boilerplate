// Library
const _ = require('lodash');
const moment = require('moment-timezone');
const ms = require('ms');
const { v4: uuidV4 } = require('uuid');
const { Duration, DateTime } = require('luxon');

// Model
const VerifyModel = require('mongoose').model('Verify');

// Services
const UserService = require('../user/user.service');

// Error

const { AppError } = require('../../core/error');
const UserError = require('../user/user.service');
const VerifyError = require('./verify.error');

// Constants
const { APP_HOST, VERIFY_ACCOUNT_TOKEN } = require('../../config/config');
const { SUBJECT, CODE, TEMPLATE_NAME, TEMPLATE_PARAMS } = require('../../utils/constants');

const verifyTokenDuration = Duration.fromMillis(ms(VERIFY_ACCOUNT_TOKEN));

const verifyTransform = ['id', 'user_email', 'type', 'code', 'expires'];

/**
 * Get verify by conditions
 * @param {Object} conditions
 */
exports.getVerifyByConditions = async (conditions) => {
    const verify = await VerifyModel.find(conditions);

    if (_.isNull(verify)) {
        throw new AppError(VerifyError.VerifyNotFound);
    }

    // Transform verify
    const transformedVerify = _.pick(verify, verifyTransform);
    return transformedVerify;
};

/**
 * Create verify
 * @param {Object} conditions
 */
exports.createVerify = async (conditions) => {
    const verify = await VerifyModel.findOne({
        user_email: conditions.user_email,
        type: conditions.type,
    });

    if (!_.isNull(verify)) {
        throw new AppError(VerifyError.VerifyAlreadyExists);
    }
    const newVerify = await VerifyModel.create({
        ...conditions,
    });

    // Transform verify
    const transformedVerify = _.pick(newVerify, verifyTransform);
    return transformedVerify;
};

/**
 * Request verify account
 * @param {String} userId
 * @param {String} userEmail
 */
exports.requestVerifyAccount = async (userId, userEmail) => {
    const user = await UserService.getUserByEmail(userId, userEmail);
    if (_.isNull(user)) {
        throw new AppError(UserError.UserNotFound);
    }

    if (_.isEqual(user.email_verified, true)) {
        throw new AppError(VerifyError.YouAlreadyVerifiedAccount);
    }
    const verify = await VerifyModel.findOne({ user_email: user.email, type: CODE.VERIFY_ACCOUNT });

    if (!_.isNull(verify)) {
        if (moment(new Date()).subtract(30, 'seconds') < verify.updated_at) {
            throw new AppError(VerifyError.YouAlreadySendEmailPleaseResendAfter30Seconds);
        }

        // Gen verify code
        const verifyCode = uuidV4().replace(/-/g, '');

        // Send email
        // agenda.sendEmail({
        //     templateName: TEMPLATE_NAME.VERIFY_ACCOUNT,
        //     templateContent: [{}],
        //     user: {
        //         email: verify.user_email,
        //     },
        //     messageConditions: {
        //         subject: SUBJECT.VERIFY_ACCOUNT,
        //         merge_vars: [
        //             {
        //                 rcpt: verify.user_email,
        //                 vars: [
        //                     {
        //                         name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.USER_FULLNAME,
        //                         content: user.name,
        //                     },
        //                     {
        //                         name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.VERIFY_URL,
        //                         content: `${APP_HOST}:5000/v1/verify-account?verify_code=${verifyCode}`,
        //                     },
        //                 ],
        //             },
        //         ],
        //     },
        // });
        // eslint-disable-next-line no-return-await
        return await VerifyModel.findOneAndUpdate(
            {
                _id: verify._id,
                user_email: verify.user_email,
                type: CODE.VERIFY_ACCOUNT,
            },
            {
                $set: {
                    code: verifyCode,
                    expires: DateTime.utc().plus(verifyTokenDuration).toJSON(),
                },
            },
            {
                new: true,
            },
        );
    }

    // Gen verify token
    const newVerify = await VerifyModel.create({
        user_email: user.email,
        type: CODE.VERIFY_ACCOUNT,
        code: uuidV4().replace(/-/g, ''),
        expires: DateTime.utc().plus(verifyTokenDuration).toJSON(),
    });

    // Send email
    // agenda.sendEmail({
    //     templateName: TEMPLATE_NAME.VERIFY_ACCOUNT,
    //     templateContent: [{}],
    //     user: {
    //         email: user.email,
    //     },
    //     messageConditions: {
    //         subject: SUBJECT.VERIFY_ACCOUNT,
    //         merge_vars: [
    //             {
    //                 rcpt: user.email,
    //                 vars: [
    //                     {
    //                         name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.USER_FULLNAME,
    //                         content: user.name,
    //                     },
    //                     {
    //                         name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.VERIFY_URL,
    //                         content: `${APP_HOST}:5000/v1/verify-account?verify_code=${newVerify.code}`,
    //                     },
    //                 ],
    //             },
    //         ],
    //     },
    // });
};
