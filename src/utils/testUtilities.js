// Model
// const UserModel = require('mongoose').model('User');
// const OrganizationModel = require('mongoose').model('Organization');
const MembershipModel = require('mongoose').model('Membership');
const ClientModel = require('mongoose').model('Client');
// const ProjectModel = require('mongoose').model('Project');

// Service
const AuthService = require('../api/auth/auth.service');

// CREATE services -------------------------------

/**
 * Create user
 * @param {object} userDto
 */
exports.createUser = (userDto) => {
    // Default client
    const client = { ip: '::1', agent: 'PostmanRuntime/7.26.5' };
    return AuthService.register({ user: userDto, client });
};

/**
 * Create client
 * @param {string} organizationId
 * @param {object} clientDto
 */
exports.createClient = async (organizationId, clientDto) => {
    const client = await ClientModel.createClient({ organization_id: organizationId, ...clientDto });
    return client;
};

/**
 * Create membership
 * @param {object} membershipDto
 */
exports.createMembership = async (organizationId, membershipDto) => {
    const membership = await MembershipModel.create({ organization_id: organizationId, ...membershipDto });
    return membership;
};
